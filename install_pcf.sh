#!/bin/sh

for d in sudo xset mkfontdir; do
    type $d || exit 1
done

srcdir="pcf"
destdir="none"

for d in /usr/share/fonts/misc /usr/share/fonts/local /usr/local/share/fonts/misc /usr/local/share/fonts/local; do
	[ -d "$d" ] && destdir="$d" && break
done

[ -d "$destdir" ] || exit 1

echo "Copying fonts to $dest. Please make sure it's in your Xserver's"
xset q | grep -A1 ^Font\ Path:

sudo cp -v "$srcdir"/*pcf.gz "$destdir/" || exit 1
sudo mkfontdir "$destdir/" || exit 1
sudo cp -v LICENSE "$destdir/terv-terc-fonts-license" || exit 1
sudo fc-cache -f # optional
xset fp rehash
